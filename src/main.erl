%%%-------------------------------------------------------------------
%%% @author main
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 06. авг. 2019 19:26
%%%-------------------------------------------------------------------
-module(main).
-author("main").

%% API
-export([print_hello_world/1]).

print_hello_world(Name) ->
  % io:fwrite().
  io:format("hello, ~s\n", [Name]),
  io:format("Good day, ~s\n", [Name]).
  